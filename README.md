# craft resolver
Output example:

```
Available resources: {stick=1, wood=64}
Crafting target:     pickaxe x3
Crafting succesfull: true
Total craft price:   {stick=1, wood=4}
Extra items crafted: {plank=3, stick=1}
```