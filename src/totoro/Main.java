package totoro;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        // demo data
        HashMap<Item, Integer> storage = new HashMap<>();
        storage.put(Items.WOOD, 64);
        storage.put(Items.STICK, 1);

        HashMap<Item, Integer> price = new HashMap<>();
        HashMap<Item, Integer> extra = new HashMap<>();

        Item target = Items.PICKAXE;
        int amount = 3;

        // now craft something
        System.out.println("Available resources: " + storage);
        System.out.println("Crafting target:     " + target + " x" + amount);
        System.out.println("Crafting succesfull: " + Crafter.craft(storage, target, amount, price, extra));
        System.out.println("Total craft price:   " + price);
        System.out.println("Extra items crafted: " + extra);
    }
}
