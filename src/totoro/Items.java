package totoro;

@SuppressWarnings({"WeakerAccess", "unused"})
public class Items {
    public static Item WOOD = new Item("wood");
    public static Item PLANKS = new Item("plank");
    public static Item STICK = new Item("stick");
    public static Item CHEST = new Item("chest");
    public static Item WORKBENCH = new Item("workbench");
    public static Item STAIR = new Item("stair");
    public static Item PICKAXE = new Item("pickaxe");
}
