package totoro;

public class Item {
    private String code;

    Item(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Item) {
            return ((Item) obj).code.equals(code);
        } else return false;
    }

    @Override
    public String toString() {
        return code;
    }
}
