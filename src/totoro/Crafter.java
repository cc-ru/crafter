package totoro;

import java.util.HashMap;

class Crafter {
    private static Integer numberOf(HashMap<Item, Integer> collection, Item item) {
        return collection == null ? 0 : collection.getOrDefault(item, 0);
    }
    private static void offset(HashMap<Item, Integer> collection, Item item, int number) {
        collection.put(item, numberOf(collection, item) + number);
    }

    static boolean craft(HashMap<Item, Integer> storage, Item item, int amount, HashMap<Item, Integer> taken, HashMap<Item, Integer> extra) {
        int fromExtra = Math.min(numberOf(extra, item), amount);
        int fromStorage = Math.min(numberOf(storage, item) - numberOf(taken, item), amount - fromExtra);
        int needToBeCrafted = amount - fromExtra - fromStorage;

        // some of necessary items my be left from previous crafts
        if (fromExtra > 0) offset(extra, item, -fromExtra);

        // some part can be taken straight from the storage
        if (fromStorage > 0) offset(taken, item, fromStorage);

        // and some part still need to be crafted from scratch
        if (needToBeCrafted > 0) {
            // search for a recipe
            Recipe recipe = Recipes.get(item);
            // if we found one - then proceed to craft
            if (recipe != null) {
                // calculate the necessary amount of crafts
                int howManyCrafts = (int) Math.ceil((double) needToBeCrafted / (double) recipe.result.number);
                // iterate over the recipe components
                for (Stack stack : recipe.grid) {
                    if (stack != null) {
                        // craft each one, multiplied to the necessary total amount of crafts
                        boolean result = craft(storage, stack.item, stack.number * howManyCrafts, taken, extra);
                        // in case of unsuccesful crafting attempt - abort the mission
                        if (!result) return false;
                    }
                }
                // as a result of this craft we can have more items than we need at the moment
                int extraAmount = howManyCrafts * recipe.result.number - needToBeCrafted;
                if (extraAmount > 0) offset(extra, item, extraAmount);
                // report success
                return true;
            }
            // if we didn't find any recipes, then sadly report failure
            else return false;
        }
        // report success
        return true;
    }
}
