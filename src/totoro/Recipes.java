package totoro;

import java.util.HashMap;

class Recipes {
    private static HashMap<Item, Recipe> recipes = new HashMap<>();

    static {
        recipes.put(
                Items.PLANKS,
                new Recipe(
                        new Stack[] {
                                new Stack(Items.WOOD, 1), null, null,
                                null, null, null,
                                null, null, null
                        },
                        new Stack(Items.PLANKS, 4)
                )
        );
        recipes.put(
                Items.WORKBENCH,
                new Recipe(
                        new Stack[] {
                                new Stack(Items.PLANKS, 1), new Stack(Items.PLANKS, 1), null,
                                new Stack(Items.PLANKS, 1), new Stack(Items.PLANKS, 1), null,
                                null, null, null
                        },
                        new Stack(Items.WORKBENCH, 1)
                )
        );
        recipes.put(
                Items.STICK,
                new Recipe(
                        new Stack[] {
                                new Stack(Items.PLANKS, 1), null, null,
                                new Stack(Items.PLANKS, 1), null, null,
                                null, null, null
                        },
                        new Stack(Items.STICK, 3)
                )
        );
        recipes.put(
                Items.PICKAXE,
                new Recipe(
                        new Stack[] {
                                new Stack(Items.PLANKS, 1), new Stack(Items.PLANKS, 1), new Stack(Items.PLANKS, 1),
                                null, new Stack(Items.STICK, 1), null,
                                null, new Stack(Items.STICK, 1), null
                        },
                        new Stack(Items.PICKAXE, 1)
                )
        );
    }

    static Recipe get(Item key) {
        return recipes.get(key);
    }
}
