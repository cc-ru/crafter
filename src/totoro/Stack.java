package totoro;

public class Stack {
    Item item;
    int number;

    Stack(Item item, int number) {
        this.item = item;
        this.number = number;
    }

    @Override
    public String toString() {
        return "(" + item + " x" + number + ")";
    }
}
